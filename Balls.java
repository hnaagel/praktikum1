public class Balls {

	enum Color {
		green, red
	};

	public static void main(String[] param) {
		// for debugging
	}

	public static void reorder(Color[] balls) {
		int greenCount = 0;
		int redCount = 0;

		// Loeme kokku palju rohelisi ja punaseid kuule on.
		for (int i = 0; i < balls.length; i++) {
			if (balls[i] == Color.green) {
				greenCount++;
			} else if (balls[i] == Color.red) {
				redCount++;
			}
		}
		
		// Juhul, kui �ht v�i teist v�rvi on 0, lopetatakse t��
		if (greenCount == 0 || redCount == 0) {
			return;
		}

		// Omistame : k�igepealt �ige arv punaseid
		for (int i = 0; i < redCount; i++) {
			balls[i] = Color.red;
		}

		// Ja �ige arv rohelisi nende otsa
		for (int i = redCount; i < redCount + greenCount; i++) {
			balls[i] = Color.green;
		}

	}
}